# AUTHOR: Cara Murphy
# COMPANY: Systems & Technology Research
# DATE: 7/22/19

import numpy as np
import tifffile as tiff
from VolumeLabelNetwork import *
from glob import glob
import imageio
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
from scipy.ndimage import filters
from matplotlib import cm

import os
from utils import multiband_io


class MaterialLabelNetwork(VolumeLabelNetwork):

    #
    # Construct the network
    #
    def __init__(self, spatialJumpCost, spatialSameCost,
                 spatialWindowCost, spatialWindowRad,
                 temporalChangeCost,temporalSameCost,matJumpCost,
                 useClasses,matColors=None,transitionOrder = None):
        super().__init__()


        if isinstance(spatialJumpCost, np.float):
            spatialJumpCost = spatialJumpCost * np.ones(4)
        if isinstance(spatialSameCost, np.float):
            spatialSameCost = spatialSameCost * np.ones(4)
        if isinstance(spatialWindowCost, np.float):
            spatialWindowCost = spatialWindowCost * np.ones(4)

        # Create factors
        self.addFactor([1, 0, 0], temporalChangeCost, temporalSameCost,
                       matJumpCost=matJumpCost)
        self.addFactor([0, 1, 0], spatialJumpCost[0], spatialSameCost[0],
                       spatialWindowCost[0], spatialWindowRad, matJumpCost)
        self.addFactor([0, 0, 1], spatialJumpCost[1], spatialSameCost[1],
                       spatialWindowCost[1], spatialWindowRad, matJumpCost)
        self.addFactor([0, 1, 1], spatialJumpCost[2], spatialSameCost[2],
                       spatialWindowCost[2], spatialWindowRad, matJumpCost)
        self.addFactor([0, 1, -1], spatialJumpCost[3], spatialSameCost[3],
                       spatialWindowCost[3], spatialWindowRad, matJumpCost)

        # Define material transition orders
        self._useClasses = useClasses
        self._transitionOrder = transitionOrder

        # Define material colormap
        if matColors is not None:
            self.setColorMap(ListedColormap(matColors,len(useClasses)))

    #
    # Create a data Cost volume from a directory of material probability images
    #
    def computeDataCost(self, image_lists, image_weights, image_dir, numClass, shadowClass, dilution=0.05):

        w = np.array(image_weights)
        if np.count_nonzero(w) == len(image_weights):
            n_total_images = sum(len(image_list) for image_list in image_lists)
        else:
            n_total_images = 0
            for idx, image_list in enumerate(image_lists):
                if image_weights[idx] != 0:
                    n_total_images += len(image_list)

        print('Total images = {}'.format(n_total_images))

        overall_image_idx = 0
        for image_type_idx, image_list in enumerate(image_lists):
            # Skip images when weight is set to zero
            if image_weights[image_type_idx] == 0:
                continue

            n_images = len(image_list)

            print('Adding {} images of type {} to cost volume'.format(n_images, image_type_idx))

            # Loop through each material probability image
            for image_idx in range(n_images):
                # geotiff_path = imageList[f]
                # material_prob = multiband_io.get_raster_bands(geotiff_path)

                for cIdx in range(len(self._useClasses)):

                    # # Load each class Cost image
                    # matFile = glob(imageList[f]+"class_"+str(self._useClasses[cIdx])+".png")
                    # cImg = imageio.imread(matFile[0]) / 255

                    regex_str = "*{}*_class_{}_*_wgs84.tif".format(image_list[image_idx], str(self._useClasses[cIdx]))
                    geotiff_path = glob(os.path.join(image_dir, regex_str))
                    cImg = multiband_io.get_raster_bands(geotiff_path[0]) / 255

                    # cImg = material_prob[self._useClasses[cIdx], :, :] / 255

                    print('<{}> is {} x {}'.format(geotiff_path, cImg.shape[0], cImg.shape[1]))
                    min_val = np.amin(cImg)
                    max_val = np.amax(cImg)
                    print("Computed Min/Max={},{}".format(min_val, max_val))

                    # Normalize
                    # if min_val == max_val:
                    #     cImg = np.zeros(cImg.shape)
                    # else:
                    #     cImg = (cImg - min_val)/(max_val - min_val)
                    #
                    # min_val = np.amin(cImg)
                    # max_val = np.amax(cImg)
                    # print("Norm Min/Max={},{}".format(min_val, max_val))

                    # Add data cost (weight by image type - default 0.7 VNIR and 0.3 SWIR)
                    cImg *= image_weights[image_type_idx]

                    min_val = np.amin(cImg)
                    max_val = np.amax(cImg)
                    print("After data weighting Computed Min/Max={},{}".format(min_val, max_val))

                    # First image operations...
                    if overall_image_idx == 0 and cIdx==0:

                        # Initialize total Cost volume
                        self._dataCost = np.zeros((n_total_images, cImg.shape[0], cImg.shape[1], numClass),
                                                  np.uint8)

                        # Initialize the invalid map
                        invalidVol = np.zeros( \
                            (n_total_images, cImg.shape[0], cImg.shape[1]), np.bool)

                        # Setup coordinate matrices
                        rCoord, cCoord = np.zeros(cImg.shape), np.zeros(cImg.shape)
                        for r in range(cImg.shape[0]):
                            rCoord[r, :] = r
                        for c in range(cImg.shape[1]):
                            cCoord[:, c] = c
                        rCoord = rCoord.astype(np.int)
                        cCoord = cCoord.astype(np.int)

                    if cIdx==0:
                        # Initialize image Cost volume
                        cCost = np.zeros((cImg.shape[0], cImg.shape[1], numClass),
                                         np.float32) + 1.0e-1
                    if cIdx!=shadowClass:
                        cCost[rCoord, cCoord, cIdx] += cImg
                    else:
                        shadowIdx = np.where(cImg>0)
                        cCost[shadowIdx[0],shadowIdx[1],:-1] += 1/numClass

                # dilute costs and save results
                cCost = (1-dilution) * cCost / np.max(cCost)
                cCost = -1 * np.log(cCost)
                cCost += -1 * np.min(cCost)

                cCost *= self._scaleFactor

                # Save the result
                self._dataCost[overall_image_idx,:,:,:] = np.uint8(cCost)
                overall_image_idx += 1

    #
    # Run belief propagation
    #
    def runBP(self, numIterations, outDir=None):

        self.initializeBP(outputDir=outDir)
        # save input label estimates
        self.saveLabelMaps(0)
        for i in range(numIterations):
            print("T-S BP iteration %d" % (i + 1))
            for f in range(self._numFactors):
                self.runBPSweep(f)
                if f == 0:
                    # update CF after temporal sweep
                    self.computeCompositeFactor()
                    # # neutralize bad bands
                    # self.dataFeedback()
                    # self.computeCompositeFactor()
            # update CF after all sweeps
            self.computeCompositeFactor()
            # save label estimates from this iteration
            self.saveLabelMaps(i + 1)

        return []

    #
    # Data feedback
    #
    def dataFeedback(self):

        costThresh = 10

        score = np.sum(np.sum(np.sum(self._dataCost[:,:,:,:-1]>costThresh,axis=-1),axis=-1),axis=-1)
        scoreThresh = 0.9*self._dataCost.shape[1]*self._dataCost.shape[2]*(self._dataCost.shape[-1]-1)
        badInd = np.where(score>scoreThresh)[0]

        for i in badInd:
            self._dataCost[i,:,:,:] = np.uint8(self._scaleFactor * (-1) * np.log(1 / self._dataCost.shape[-1]))

    #
    # Change detection
    #
    def changeDetectionMap(self, outDir, natural, manmade, iter):

        outputList = sorted(glob(outDir + "output_%02d_*.tif" % iter))

        for f in range(len(outputList)):
            output = tiff.imread(outputList[f])
            if f==0:
                prev = np.copy(output)
                changeMap = -1 * np.ones(output.shape)
                changeMap_semantic = -1 * np.ones(output.shape)
                continue
            changeMap_semantic[np.where(np.logical_and(np.isin(prev,natural),np.isin(output,manmade)))] = f /len(outputList) * 0.95
            changeMap_semantic[np.where(np.logical_and(np.isin(prev, manmade), np.isin(output, natural)))] = -1
            changeMap[np.where(prev != output)] = f /len(outputList) * 0.95
            # changeMap[np.where(np.logical_and(np.isin(prev, manmade), np.isin(output, natural)))] = -1
            prev = np.copy(output)

        # set the colormap
        changeScale = LinearSegmentedColormap.from_list(
            'trunc({n},{a:.2f},{b:.2f})'.format(n=cm.gray, a=0, b=1),
            cm.gray(np.linspace(.99, 1, len(outputList))))
        changeScale.set_under('k')

        # changeScale = cm.jet()
        # changeScale.set_under('w')
        # save results
        cImg = changeScale(changeMap)
        # cImg = cm.CMRmap(changeMap)
        cImg = (255 * cImg).astype(np.uint8)
        tiff.imwrite(outDir + "changeMap_" + str(iter).zfill(2) + ".tif", cImg)

        # save results
        cImg = changeScale(changeMap_semantic)
        # cImg = cm.CMRmap(changeMap_semantic)
        cImg = (255 * cImg).astype(np.uint8)
        tiff.imwrite(outDir + "changeMap_semantic_" + str(iter).zfill(2) + ".tif", cImg)