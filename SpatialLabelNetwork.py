# AUTHOR: Cara Murphy
# COMPANY: Systems & Technology Research
# DATE: 9/30/19

from VolumeLabelNetwork import *
import cv2

class SpatialLabelNetwork(VolumeLabelNetwork):

    #
    # Construct the network
    #
    def __init__(self,spatialSameCost,spatialWindowCost, spatialWindowRad,
                 spatialJumpCost=100.0, gradientSource=None,cmap=None,scaleFactor=1):

        super().__init__()

        # Change color map for output images
        if cmap is not None:
            self.setColorMap(cmap)

        # adjust scale factor for stereo inputs
        self.setScaleFactor(scaleFactor)

        # pre-calculate gradient spatial jump cost
        if gradientSource is not None:
            spatialJumpCost=self.calculateGradientCost(gradientSource)

        if isinstance(spatialJumpCost, np.float):
            spatialJumpCost = spatialJumpCost * np.ones(4)
        if isinstance(spatialSameCost, np.float):
            spatialSameCost = spatialSameCost * np.ones(4)
        if isinstance(spatialWindowCost, np.float):
            spatialWindowCost = spatialWindowCost * np.ones(4)

        # update spatialJumpCost to gradient-based

        # Create spatial factors
        self.addFactor([0, 1, 0], spatialJumpCost[0], spatialSameCost[0],
                       spatialWindowCost[0], spatialWindowRad)
        self.addFactor([0, 0, 1], spatialJumpCost[1], spatialSameCost[1],
                       spatialWindowCost[1], spatialWindowRad)
        self.addFactor([0, 1, 1], spatialJumpCost[2], spatialSameCost[2],
                       spatialWindowCost[2], spatialWindowRad)
        self.addFactor([0, 1, -1], spatialJumpCost[3], spatialSameCost[3],
                       spatialWindowCost[3], spatialWindowRad)

    def calculateGradientCost(self,im):

        # Gradient-based spatial transition costs
        mMaxGrad = 16
        stdDevUnit = 64
        p1StdDev = 0.5
        p2MinStdDev = 2
        p2MaxStdDev = 8
        mMinP2 = p2MinStdDev * stdDevUnit
        mMaxP2 = p2MaxStdDev * stdDevUnit
        gradKernelSize = np.int(3)
        sqrt2Norm = 1.0 / np.sqrt(2.0)
        gradNorm = 1.0 / mMaxGrad
        sobelNorm = 1 / (2 ** (2 * gradKernelSize - 3))
        numFactors = 4

        # compute gradients for transition probabilities
        gradX = cv2.Sobel(im, cv2.CV_32F, 1, 0, ksize=gradKernelSize, scale=sobelNorm)
        gradY = cv2.Sobel(im, cv2.CV_32F, 0, 1, ksize=gradKernelSize, scale=sobelNorm)

        derivImg = np.zeros((1, im.shape[0], im.shape[1], numFactors))

        g0 = abs(gradY) * gradNorm
        g0[np.where(g0 > 1)] = 1
        derivImg[:, :, :, 0] = g0
        g1 = abs(gradX) * gradNorm
        g1[np.where(g1 > 1)] = 1
        derivImg[:, :, :, 1] = g1
        g2 = abs(sqrt2Norm * (gradX + gradY)) * gradNorm
        g2[np.where(g2 > 1)] = 1
        derivImg[:, :, :, 2] = g2
        g3 = abs(sqrt2Norm * (gradX - gradY)) * gradNorm
        g3[np.where(g3 > 1)] = 1
        derivImg[:, :, :, 3] = g3

        spatialJumpCost = []
        for g in range(numFactors):
            p2 = mMaxP2 + (mMinP2 - mMaxP2) * (derivImg[:, :, :, g])
            spatialJumpCost.append(p2)

        return spatialJumpCost

    #
    # Run belief propagation
    #
    def runBP(self, numIterations, frame, outDir = None):

        self.initializeBP(outputDir=outDir)
        # save input label estimates
        self.saveLabelMaps(0,fileNum=frame)
        for i in range(numIterations):
            print("S BP iteration %d" % (i+1))
            for f in range(self._numFactors):
                self.runBPSweep(f)
            # update CF after all sweeps
            self.computeCompositeFactor()
            # save label estimates from this iteration
            self.saveLabelMaps(i+1,fileNum=frame)
            # self.dumpVolume("C:/results/bpexp/"+"dump/b")

        return []