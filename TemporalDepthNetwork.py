# AUTHOR: Thomas Pollard
# COMPANY: Systems & Technology Research
# DATE: 6/28/19

import numpy as np
import tifffile as tiff
import re
from scipy.ndimage import filters
from matplotlib import cm
from VolumeLabelNetwork import *
from matplotlib.colors import LinearSegmentedColormap

class TemporalDepthNetwork(VolumeLabelNetwork):

    #
    # Construct the network
    #
    def __init__(self):
        super().__init__()

    def initializeFactors(self,spatialJumpCost, spatialSameCost,
                 spatialWindowCost, spatialWindowRad,
                 temporalChangeCost0,temporalSameCost = 0.0):

        if self._timeStamps is not None:
            gradT = np.diff(self._timeStamps).astype('float32')+1e-2
            temporalChangeCost = np.copy(gradT)
            temporalChangeCost = temporalChangeCost / np.max(temporalChangeCost)
            temporalChangeCost[np.where(temporalChangeCost>=1.0)[0]] = 0.99
            temporalChangeCost = -1*np.log(temporalChangeCost)+temporalChangeCost0
            # temporalSameCost = gradT/np.max(gradT)*temporalChangeCost0**2
        else:
            temporalChangeCost = temporalChangeCost0

        if isinstance(spatialJumpCost, np.float):
            spatialJumpCost = spatialJumpCost * np.ones(4)
        if isinstance(spatialSameCost, np.float):
            spatialSameCost = spatialSameCost * np.ones(4)
        if isinstance(spatialWindowCost, np.float):
            spatialWindowCost = spatialWindowCost * np.ones(4)

        # Create factors
        self.addFactor([1, 0, 0], temporalChangeCost, temporalSameCost)
        self.addFactor([0, 1, 0], spatialJumpCost[0], spatialSameCost[0],
                       spatialWindowCost[0], spatialWindowRad)
        self.addFactor([0, 0, 1], spatialJumpCost[1], spatialSameCost[1],
                       spatialWindowCost[1], spatialWindowRad)
        self.addFactor([0, 1, 1], spatialJumpCost[2], spatialSameCost[2],
                       spatialWindowCost[2], spatialWindowRad)
        self.addFactor([0, 1, -1], spatialJumpCost[3], spatialSameCost[3],
                       spatialWindowCost[3], spatialWindowRad)

    #
    # Create a data Cost volume from a directory of height map images
    #
    def computeDataCost(self,heightFiles,dilution=0.1,averaging=None):

        # averaging = 'input' for averaging on input images
        # averaging = 'cost' for averaging on data cost volume
        # averaging = None for no averaging

        # time info
        t0 = np.zeros((len(heightFiles)), dtype=int)
        ind0 = [a.start() for a in list(re.finditer('/', heightFiles[0]))][-1] + 1
        ind1 = [a.start() for a in list(re.finditer('_', heightFiles[0]))][-1]
        for f in range(len(heightFiles)):
            t0[f] += int(heightFiles[f][ind0:ind1])
        if averaging is not None:
            # find unique timeStamps and averaging
            uTime, uInd = np.unique(t0, return_inverse=True)
        else:
            uTime = np.copy(t0)
            uInd = np.linspace(0, len(heightFiles) - 1, len(heightFiles))
        self.setTimeStamps(uTime)

        # height info
        numZ = int((self._maxLabel - self._minLabel) / self._stepLabel)
        stdev = self._stdevLabel / self._stepLabel
        gaussRad = int(np.rint(2 * stdev))
        numZPadded = numZ + 2 * gaussRad + 1
        invalidIdx = numZPadded - 1

        # Loop through each unique time
        for u in range(len(uTime)):

            ind = np.where(uInd==u)[0]

            for f in range(len(ind)):

                # Load the height image
                hImg = tiff.imread(heightFiles[ind[f]])

                # First image operations...
                if u == 0 and f == 0:

                    # Initialize total Cost volume
                    self._dataCost = np.zeros((len(uTime), hImg.shape[0], hImg.shape[1], numZ),
                                              np.uint8)

                    # Initialize the invalid map
                    self._invalidVol = np.zeros( \
                        (len(uTime), hImg.shape[0], hImg.shape[1]), np.bool)

                    # Setup coordinate matrices
                    rCoord, cCoord = np.zeros(hImg.shape), np.zeros(hImg.shape)
                    for r in range(hImg.shape[0]):
                        rCoord[r, :] = r
                    for c in range(hImg.shape[1]):
                        cCoord[:, c] = c
                    rCoord = rCoord.astype(np.int)
                    cCoord = cCoord.astype(np.int)

                if f == 0:

                    # Initialize image Cost volume
                    if averaging == 'input':
                        hCost = np.zeros((1, hImg.shape[0], hImg.shape[1], numZPadded),
                                     np.float32) + 1e-2
                        hInvalid = np.zeros(( 1, hImg.shape[0], hImg.shape[1]), np.bool)
                    else:
                        hCost = np.zeros((len(ind), hImg.shape[0], hImg.shape[1], numZPadded),
                                         np.float32) + 1e-2
                        hInvalid = np.zeros((len(ind), hImg.shape[0], hImg.shape[1]), np.bool)

                # averaging inputs and continue
                if averaging=='input':
                    if f>0:
                        hImg += tiff.imread(heightFiles[ind[f]])
                    if f<len(ind)-1:
                        continue
                    else:
                        hImg /= len(ind)
                        im = 0
                else:
                    im = f

                # Convert height map to index in padded volume
                hIdx = np.clip(np.rint((hImg - self._minLabel) / self._stepLabel), 0, numZ - 1)
                invalidMap = np.logical_or(np.isnan(hImg),hImg == 0)
                hInvalid[im,:,:] += np.copy(invalidMap)
                hIdx[invalidMap] = invalidIdx

                # Add a Gaussian Cost around the height
                for r in range(2 * gaussRad + 1):
                    dr = r - gaussRad
                    gaussVal = np.exp(-dr * dr / (2 * pow(stdev, 2)))

                    hCost[im, rCoord, cCoord, hIdx.astype(np.int)] += gaussVal

                    hIdx[np.invert(invalidMap)] += 1

                invalidIdxList = np.where(invalidMap > 0)
                hCost[im, invalidIdxList[0], invalidIdxList[1], :] += 1 / numZ

                # dilute costs and save results
                hCost[im,:,:,:] = (1 - dilution) * hCost[im,:,:,:] / np.max(hCost[im,:,:,:])
                hCost[im,:,:,:] = -1 * np.log(hCost[im,:,:,:])
                hCost[im,:,:,:] += -1 * np.min(hCost[im,:,:,:])

            if len(ind)>1 and averaging=='cost':
                invalidInd0 = np.where(hInvalid==1)
                hCost[invalidInd0[0],invalidInd0[1],invalidInd0[2],:] = np.nan
                dataCounts = np.sum(hCost>0,axis=0)
                hCost = np.nansum(hCost,axis=0) / dataCounts
                invalidInd1 = np.where(np.isnan(hCost))
                hCost[invalidInd1[0],invalidInd1[1],:] = -1*np.log(1/numZ)
            else:
                hCost = hCost[0,:,:,:]

            self._dataCost[u,:,:,:] += np.uint8(self._scaleFactor * hCost[:, :, gaussRad:numZ + gaussRad])
            self._invalidVol[u,:,:] = np.min(hInvalid,axis=0)

        return []

    #
    # Run belief propagation
    #
    def runBP(self, numIterations, outDir = None):

        self.initializeBP(outputDir=outDir)
        # save input label estimates
        self.saveLabelMaps(0)
        self.changeDetectionMap(0)
        for i in range(numIterations):
            print("T-S BP iteration %d" % (i+1))
            for f in range(self._numFactors):
                self.runBPSweep(f)
                if f==0:
                    # update CF afer temporal sweep
                    self.computeCompositeFactor()
            # update CF after all sweeps
            self.computeCompositeFactor()
            # save label estimates from this iteration
            self.saveLabelMaps(i+1)
            # save change detection map
            self.changeDetectionMap(i+1)
            # self.dumpVolume("C:/results/bpexp/"+"dump/b")

        return []

    #
    # Change detection
    #
    def changeDetectionMap(self,iter):

        # extract labels
        minLabels, _ = self.extractMinCostLabels()
        output = (self._minLabel + minLabels * self._stepLabel).astype(np.float32)

        # initialize maps
        minMap = filters.minimum_filter(output[0, :, :], size=5)
        maxMap = filters.maximum_filter(output[0, :, :], size=5)
        changeMap = -1*np.ones(minMap.shape)

        # set the colormap
        changeScale = LinearSegmentedColormap.from_list(
            'trunc({n},{a:.2f},{b:.2f})'.format(n=cm.bone, a=0, b=0.95),
            cm.bone(np.linspace(0, 0.95, output.shape[0])))
        changeScale.set_under('w')

        # change detection
        for t in range(output.shape[0]):
            changeMap[np.logical_and(np.logical_or(output[t, :, :] > maxMap, output[t, :, :] < minMap), changeMap < 0.0)] = (t+1)/output.shape[0]*0.95

        # save results
        cImg = changeScale(changeMap[:,:])
        cImg = (255 * cImg).astype(np.uint8)
        tiff.imwrite(self._outputDir + "changeMap_" + str(iter).zfill(2) + ".tif", cImg)

