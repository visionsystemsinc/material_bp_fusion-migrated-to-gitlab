# AUTHOR: Thomas Pollard
# COMPANY: Systems & Technology Research
# DATE: 6/26/19

import os
import numpy as np
import tifffile as tiff
from matplotlib import cm

#
# An abstract class for performing belief-propagation (BP) inference on a volume
# of labels with factors defined between adjacent voxels. 
#

class VolumeLabelNetwork:

    #
    # Construct empty
    #
    def __init__(self):

        # Factors
        self._numFactors = 0
        self._factorStep = []
        self._f2v = []
        self._cf = []

        # Transition Costs
        self._jumpCost = []
        self._sameCost = []
        self._windowCost = []
        self._windowRad = []
        self._timeStamps = None

        # Material-specific transition cost
        self._matJumpCost = []

        # Data
        self._dataCost = []
        self._invalidVol = []
        self._labelAxis = 3
        self._timeAxis = 0

        # Label params
        self._minLabel = []
        self._maxLabel = []
        self._stepLabel = []
        self._stdevLabel = []

        # Scale factor
        self._scaleFactor = 10

        # Results
        self._outputDir = None
        self._cmap = cm.jet

    #
    # Save and load data cost and invalid map
    #

    def saveDataCost(self, dataCostFile, invalidFile, timeStampFile=None):
        np.save(dataCostFile, self._dataCost.astype('float32') / np.max(self._dataCost)*255)
        np.save(invalidFile, self._invalidVol)
        if timeStampFile is not None:
            np.save(timeStampFile, self._timeStamps)

    def initializeCost(self,cost,invalidMap=None):
        self._dataCost = np.uint8(np.round(cost*self._scaleFactor))
        if invalidMap is not None:
            self._invalidVol = invalidMap

    def loadDataCost(self, dataCostFile, invalidFile=None, timeStampFile=None):

        dataCost = np.load(dataCostFile, allow_pickle=True) / 255

        if timeStampFile is not None:
            timeStamps = np.load(timeStampFile)
            self.setTimeStamps(timeStamps)

        if invalidFile==None:
            self._invalidVol = np.zeros(dataCost.shape[:-1])
            for t in range(dataCost.shape[0]):
                invalidMap = np.any(np.isnan(dataCost[t, :, :, :], axis=2))
                self._invalidVol[t, :, :] = invalidMap
        else:
            self._invalidVol = np.load(invalidFile)
        self.initializeCost(dataCost)

    def diluteCost(self,cost,dilution=0.1):
        prob = np.exp(-1*cost.astype('float32'))
        cost = -1*np.log(dilution + (1.0 - dilution) * prob / np.max(prob))
        cost += -1*np.min(cost)
        cost *= self._scaleFactor

        return cost

    def saveLabelMaps(self,iter,fileNum=None):

        if self._outputDir is None:
            print('WARNING: outputDir must be specified in initializeBP; aborting saveLabelMaps.')
        else:
            minLabels, _ = self.extractMinCostLabels()
            output = (self._minLabel + minLabels * self._stepLabel).astype(np.float32)
            slice = [np.s_[:], np.s_[:], np.s_[:]]
            for t in range(output.shape[self._timeAxis]):
                if fileNum is None or t>0:
                    fileNum = np.int(t)
                slice[self._timeAxis] = np.int(t)
                tiff.imwrite(self._outputDir + "output_" + str(iter).zfill(2) + "_" + str(fileNum).zfill(2) + ".tif", output[tuple(slice)])
                cImg = self._cmap((output[tuple(slice)] - self._minLabel) / (self._maxLabel - self._minLabel))
                cImg = (255 * cImg).astype(np.uint8)
                tiff.imwrite(self._outputDir + "colormap_" + str(iter).zfill(2) + "_" + str(fileNum).zfill(2) + ".tif", cImg)

    #
    # Change the scale factor variable
    #
    def setScaleFactor(self,scaleFactor):

        self._scaleFactor = scaleFactor

    #
    # Change the color map
    #
    def setColorMap(self, cmap):

        self._cmap = cmap

    #
    # Set the timeStamps array
    #
    def setTimeStamps(self,timeStamps):

        self._timeStamps = timeStamps

    #
    # Set label params
    #
    def setLabelParams(self,minL,maxL,stepL=1,stdevL=1):

        self._minLabel = minL
        self._maxLabel = maxL
        self._stepLabel = stepL
        self._stdevLabel = stdevL

    #
    # Add a new factor connecting all voxels in the factorStep direction.
    #
    # Define a simplified transition Cost matrix between neighboring
    # voxels in the factor by defining:
    #   jumpCost: Cost for transitioning to any other label
    #   sameCost: Cost for having the same label
    #   winCost (optional): label transitions in a sliding window have this Cost
    #   winRad (optional): radius of the sliding window
    #
    def addFactor(self, factorStep, jumpCost, sameCost,
                  winCost=0.0, winRad=0, matJumpCost=None):

        if(not len(factorStep)==3):
            print("ERROR: factorStep should be 3-dim vector, factor not added")
            
        self._numFactors += 1
        self._factorStep.append(factorStep)

        self._jumpCost.append(np.uint16(np.round(jumpCost*self._scaleFactor)))
        self._sameCost.append(np.uint16(np.round(sameCost*self._scaleFactor)))
        self._windowCost.append(np.uint16(np.round(winCost*self._scaleFactor)))
        self._windowRad.append(winRad)
        if matJumpCost is not None:
            self._matJumpCost.append(np.uint16(np.round(matJumpCost*self._scaleFactor)))

    #
    # Run belief propagation
    #
    def runBP(self, numIterations, outDir=None):

        self.initializeBP(outDir)
        # save input label estimates
        self.saveLabelMaps(0)
        for i in range(numIterations):
            print("BP iteration %d" % (i+1))
            for f in range(self._numFactors):
                self.runBPSweep(f)
            self.computeCompositeFactor()
            # save label estimates from this iteration
            self.saveLabelMaps(i + 1)
            # self.dumpVolume("C:/results/bpexp/"+"dump/b")

    #
    # Allocate and initialize memory for belief propagation
    #
    def initializeBP(self, outputDir=None):

        # Initialize the composite factor
        self._cf = np.copy(self._dataCost)

        # Allocate memory for factor to variable messages
        self._f2v = []
        for f in range(self._numFactors):
            self._f2v.append(np.zeros(self._dataCost.shape, np.uint16))

        if outputDir is not None:
            self._outputDir = outputDir
        
    #
    # Sweep along a factor axis, updating factor-to-variable messages and
    # variable-to-factor messages at each step, for faster convergence.  Set
    # reverse=True to sweep backwards along the axis.  Multiple calls of this
    # function along different axes amounts to a max-product belief propagation
    # algorithm with asynchronous scheduling.
    #
    def runBPSweep(self, factorIdx):

        # Determine an axis to sweep over
        sweepAxis = np.argmax(np.abs(self._factorStep[factorIdx]))

        # Pre-populate sweeping indices
        n = self._dataCost.shape[sweepAxis]
        nl = self._dataCost.shape[self._labelAxis]
        sStep = self._factorStep[factorIdx][sweepAxis]
        if sStep>0:
            sweepInd = np.arange(0,n,sStep,dtype='int')
        else:
            sweepInd = np.arange(n+sStep,-1, sStep, dtype='int')
        ns = len(sweepInd)

        # Find any other axes with non-zero step and pre-populate indices
        nonZeroAxes = np.setdiff1d(np.nonzero(self._factorStep[factorIdx])[0],sweepAxis)

        currInd = []
        prevInd = []
        for a in nonZeroAxes:

            aStep = self._factorStep[factorIdx][a]
            aN = self._dataCost.shape[a]

            currInd.append(np.arange(np.max((aStep, 0)), aN + np.min((aStep, 0))))
            prevInd.append(np.arange(abs(np.min((aStep, 0))), aN - np.max((aStep, 0))))

        # Pre-compute partial variable-to-factor messages for each voxel
        v2fPartial = self._cf - self._f2v[factorIdx]

        # Setup sweep for forward and backward
        for reverse in range(2):

            f2vUpdate = np.zeros(self._dataCost.shape, np.uint16)

            # Begin sweep
            #invalidMap = np.copy(self._invalidVol)
            for s in range(1,len(sweepInd)):

                # Determine message slice indices
                cs = [np.s_[:], np.s_[:], np.s_[:], np.s_[:]]
                ps = [np.s_[:], np.s_[:], np.s_[:], np.s_[:]]

                if reverse:
                    cs[sweepAxis] = sweepInd[ns-s-1]
                    ps[sweepAxis] = sweepInd[ns-s]
                else:
                    cs[sweepAxis] = sweepInd[s]
                    ps[sweepAxis] = sweepInd[s-1]

                # add indices for other non-zero axes
                for a in range(len(currInd)):

                    if reverse:
                        cs[nonZeroAxes[a]] = prevInd[a]
                        ps[nonZeroAxes[a]] = currInd[a]
                    else:
                        cs[nonZeroAxes[a]] = currInd[a]
                        ps[nonZeroAxes[a]] = prevInd[a]

                curSlice = (cs[0], cs[1], cs[2], cs[3])
                prevSlice = (ps[0], ps[1], ps[2], ps[3])

                # Compute complete variable to factor message along sweep axis
                # and normalize
                prevCost = v2fPartial[prevSlice] + f2vUpdate[prevSlice]
                minPrevCost = np.min(prevCost, axis=2)
                prevCost = prevCost - minPrevCost[:, :, None]

                # temporal jump cost works a little differently...
                if sweepAxis==self._timeAxis and not isinstance(self._jumpCost[factorIdx],np.uint16):
                    jumpCost = self._jumpCost[factorIdx][cs[sweepAxis]-1+reverse]
                    # sameCost = self._sameCost[factorIdx][cs[sweepAxis]-1+reverse]
                else:
                    jumpCost = self._jumpCost[factorIdx]
                sameCost = self._sameCost[factorIdx]

                # Reshape scalar transition costs to arrays
                if isinstance(jumpCost,np.uint16):
                    jumpCost = jumpCost * np.ones(prevCost.shape[0:-1], np.uint16)
                else:
                    jumpCost = jumpCost[prevSlice[0:-1]]
                if isinstance(sameCost,np.uint16):
                    sameCost = sameCost * np.ones(prevCost.shape[0:-1], np.uint16)
                else:
                    sameCost = self._sameCost[factorIdx][prevSlice[0:-1]]
                if isinstance(self._windowCost[factorIdx],np.uint16):
                    windowCost = self._windowCost[factorIdx] * np.ones(prevCost.shape[0:-1], np.uint16)
                else:
                    windowCost = self._windowCost[factorIdx][prevSlice[0:-1]]

                # Initialize factor-to-variable messages to the jump cost
                if (len(self._matJumpCost) > 0) and (self._transitionOrder is not None) and isinstance(self._matJumpCost[factorIdx],np.ndarray):
                    # print("WARNING: invalid entries are not currently considered when using transition cost arrays!")
                    prevLabel = np.argmin(prevCost, axis=self._labelAxis - 1)
                    for t in range(len(self._transitionOrder)):
                        idx = np.unravel_index(np.where(np.in1d(prevLabel, self._transitionOrder[t])), prevLabel.shape[0:2])
                        transitionCost = (prevCost+jumpCost[:,:,None])[idx[0], idx[1], 0:-1] + self._matJumpCost[factorIdx][t]
                        f2vUpdate[curSlice][idx[0], idx[1], :] = np.repeat(np.expand_dims(np.amin( \
                            transitionCost, \
                            self._labelAxis - 1), \
                            self._labelAxis - 1), prevCost.shape[self._labelAxis - 1], \
                            axis=self._labelAxis - 1)
                else:
                    f2vUpdate[curSlice] = np.repeat(np.expand_dims(np.amin( \
                        jumpCost[:,:,None] + prevCost, \
                        self._labelAxis - 1), \
                        self._labelAxis - 1), prevCost.shape[self._labelAxis - 1], \
                        axis=self._labelAxis - 1)

                # Update messages with same label costs
                f2vUpdate[curSlice] = np.minimum( \
                    f2vUpdate[curSlice], sameCost[:,:,None] + prevCost)

                # Update messages with sliding window transition, if any
                for dl in range(self._windowRad[factorIdx] + 1):
                    if dl == 0: continue

                    # +dl
                    curSlice = (curSlice[0], curSlice[1], curSlice[2], np.s_[0:nl - dl])
                    f2vUpdate[curSlice] = np.minimum(f2vUpdate[curSlice], \
                                                             windowCost[:,:,None] + prevCost[:, :, np.s_[dl:nl]])

                    # -dl
                    curSlice = (curSlice[0], curSlice[1], curSlice[2], np.s_[dl:nl])
                    f2vUpdate[curSlice] = np.minimum(f2vUpdate[curSlice], \
                                                             windowCost[:,:,None] + prevCost[:, :, np.s_[0:nl - dl]])
                # if factorIdx==self._timeAxis:
                #     for i in range(f2vUpdate.shape[-1]):
                #         f2vUpdate[curSlice][:,:,i][self._invalidVol[cs[0]]] = np.copy(prevCost[:,:,i][self._invalidVol[cs[0]]])

            # Store the new factor-to-variable messages
            if reverse:
                self._f2v[factorIdx] += f2vUpdate
            else:
                self._f2v[factorIdx] = np.copy(f2vUpdate)

        return []


     # Sum the data and other factors into a composite
    def computeCompositeFactor(self):

        self._cf = np.uint16(self._dataCost)
        
        for f in range(self._numFactors):
            self._cf = self._cf + self._f2v[f].astype('uint16')

        return []


    #
    # Extract the label with minimum cost for every pixel in every image
    #
    def extractMinCostLabels(self):

        # Take arg min
        minLabel = np.argmin(self._cf, axis=self._labelAxis)
        minCost = np.amin(self._cf, axis=self._labelAxis).astype('float')

        return [minLabel, minCost]

    #
    # Write one or all slices of the composite factor volume to disk as a
    # stack of images
    #
    def dumpVolume(self, outNamebase, gain=1.0, offset=0.0,
                   sliceAxis1=0, sliceIdx1=-1,
                   sliceAxis2=3, sliceIdx2=-1):

        if sliceAxis1 < 0 or sliceAxis1 > self._labelAxis: return
        if sliceAxis2 < 0 or sliceAxis2 > self._labelAxis: return

        for s1 in range(self._cf.shape[sliceAxis1]):
            if sliceIdx1 >= 0 and not s1 == sliceIdx1: continue
            for s2 in range(self._cf.shape[sliceAxis2]):
                if sliceIdx2 >= 0 and not s2 == sliceIdx2: continue

                sp = [np.s_[:], np.s_[:], np.s_[:], np.s_[:]]
                sp[sliceAxis1], sp[sliceAxis2] = s1, s2
                rawSlice = self._cf[(sp[0], sp[1], sp[2], sp[3])]

                sliceImg = np.clip(gain * rawSlice + offset, 0, 1.0)
                sliceVis = cm.jet(sliceImg)

                sliceName = outNamebase + "_" + str(s1) + " " + str(s2) + ".tif"
                tiff.imwrite(sliceName, (255 * sliceVis).astype(np.uint8))
