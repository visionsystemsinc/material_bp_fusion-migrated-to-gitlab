# AUTHOR: Cara Murphy
# COMPANY: Systems & Technology Research
# DATE: 12/23/19

import imageio
from SpatialLabelNetwork import *
from matplotlib import cm

# depth files
# depthDir = "C:/results/model_capitol5/stereo/"
# outDir = "C:/results/capitol_volumes/"
depthDir = "/Users/cara.perkins/Desktop/STR_Programs/SatTel/data/capitol_volumes/"
outDir = "/Users/cara.perkins/Desktop/STR_Programs/SatTel/results/beliefPropagation/capitol_volumes/"

spatialSameCost = 0.0
spatialWindowRad = np.int_(1)
spatialWindowCost = 32.0

numFactors = 4
numFiles = 17

for f in range(numFiles):

    # get volume data
    volFile = depthDir+"photo%d" % f
    im = imageio.imread(volFile + "_target.png")
    rows,cols = im.shape
    rawData = np.fromfile(volFile,dtype=np.uint8,sep="")
    depths = np.int_(rawData.shape[0]/(rows*cols))
    vol = np.reshape(rawData,(1,rows,cols,depths))
    minZ, maxZ = 0, vol.shape[-1]-1

    # get invalid map
    invalidMap = np.any(vol>=255,axis=-1)

    # update invalid costs
    invalidIdxList = np.where(invalidMap[0, :, :] > 0)
    vol[0, invalidIdxList[0], invalidIdxList[1], :] = 1

    # set up the network
    net = SpatialLabelNetwork(spatialSameCost, spatialWindowCost, spatialWindowRad,gradientSource=im,cmap=cm.gray,scaleFactor=1)
    # set parameters
    net.setLabelParams(minZ, maxZ)
    # initialize data cost
    net.initializeCost(vol, invalidMap)

    print("Running belief propagation")
    net.runBP(3,f,outDir)

