# AUTHOR: Cara Murphy
# COMPANY: Systems & Technology Research
# DATE: 7/22/19

import os
from glob import glob
import numpy as np
import matplotlib.pyplot as plt

from MaterialLabelNetwork import *


# imageType = 'Landsat' #'Worldview' #
imageType = 'Worldview'

# class list and colors
classList = ['grass',  # 0
             'glass',  # 1
             'soil',  # 2
             'water',  # 3
             'concrete',  # 4
             'brick',  # 5
             'asphalt',  # 6
             'metal',  # 7
             'solar panel',  # 8
             'tree',  # 9
             'polymer',  # 10
             'shadow']  # 11

allColors = {
    'grass': [0, 1, 0],
    'glass': [0, 0.5, 0.5],
    'soil': [0.85, 0.8, 0.5], #[1, 1, 0], #
    'water': [0, 0, 1],
    'concrete': [0.66, 0.66, 0.66],
    'brick': [1, 0, 0],
    'asphalt': [0.33, 0.33, 0.33],
    'metal': [0, 1, 1],
    'solar panel': [0.54, 0.17, 0.87],
    'tree': [0.13, 0.55, 0.13],
    'polymer': [1, 1, 1],
    'shadow': [0, 0, 0]
}

visColors = np.zeros((len(classList), 3))
for k, color in enumerate(classList):
    visColors[k, :] = allColors[color]


if imageType == 'Worldview':

    # imageList = sorted(glob("C:/Users/rwagner/sandbox/core3d/sandboxData/delivery19/images/*_uint8.tif"))
    # imageList = ['16SEP25165422-M1BS-500916291030_01_P001', '16OCT19164337-M1BS-500952819010_01_P001',
    #              '16NOV07165044-M1BS-500985234010_01_P001', '16OCT07164856-M1BS-500941044030_01_P001',
    #              '16NOV13164808-M1BS-500969558010_01_P001', '16SEP13170005-M1BS-500879858010_01_P001']
    #
    # imageList = ['16SEP19165732-A1BS-500901283010_01_P002', '16OCT14170045-A1BS-500910627010_01_P002',
    #              '16AUG30163357-A1BS-500849095010_01_P002', '16OCT19164335-A1BS-500952819010_01_P002',
    #              '6OCT26165541-A1BS-500959534010_01_P001', '16SEP13170007-A1BS-500879858010_01_P002']

    # D3 Imagelist
    imageList = ['15JUL26183742-M1BS-500647758050_01_P001', '15JAN05183041-M1BS-500647758090_01_P001',
                 '15DEC18183806-M1BS-500647761050_01_P001', '15FEB06184344-M1BS-500647761030_01_P001',
                 '16FEB20184727-M1BS-500647758030_01_P001', '15AUG14184110-M1BS-500647760010_01_P001',
                 '17MAY14190109-M1BS-501272522010_01_P001', '15APR28183136-M1BS-500647759100_01_P001',
                 '15JUL27185400-M1BS-500647759030_01_P001', '15JAN24183547-M1BS-500647760080_01_P001',
                 '16FEB08185810-M1BS-500647761040_01_P001', '14NOV09182948-M1BS-500647758070_01_P001',
                 '17AUG09185853-M1BS-501465516010_01_P002', '17AUG16191140-M1BS-501522381010_01_P001',
                 '15APR29184709-M1BS-500647759090_01_P001', '14OCT27182510-M1BS-500647758060_01_P001',
                 '14OCT28184008-M1BS-500647759080_01_P001', '15NOV17184802-M1BS-500647761060_01_P001',
                 '14NOV22183428-M1BS-500647758040_01_P001', '16FEB14185256-M1BS-500647761020_01_P001',
                 '15DEC13185848-M1BS-500647759060_01_P001', '15DEC31184343-M1BS-500647759070_01_P001',
                 '14NOV28182901-M1BS-500647760090_01_P001', '15NOV23184304-M1BS-500647760020_01_P001',
                 '15FEB11182349-M1BS-500647759020_01_P001', '15MAR21182922-M1BS-500647758100_01_P001',
                 '15JAN05183058-M1BS-500230054020_01_P003', '15DEC25184902-M1BS-500647759050_01_P001',
                 '15AUG27184856-M1BS-500647760040_01_P001', '16JAN01185802-M1BS-500647760070_01_P001',
                 '16JAN26185338-M1BS-500647760100_01_P001', '15JAN05182953-M1BS-500230054010_01_P002',
                 '14DEC23182236-M1BS-500647759010_01_P001', '15OCT23185312-M1BS-500647761070_01_P001',
                 '15APR10184641-M1BS-500647760060_01_P001', '15FEB12183926-M1BS-500647760030_01_P001',
                 '15SEP28185751-M1BS-500647760050_01_P001', '15DEC25184902-M1BS-500647759050_01_P001',
                 '15AUG27184856-M1BS-500647760040_01_P001', '16JAN01185802-M1BS-500647760070_01_P001',
                 '16JAN26185338-M1BS-500647760100_01_P001', '15JAN05182953-M1BS-500230054010_01_P002',
                 '14DEC23182236-M1BS-500647759010_01_P001', '15OCT23185312-M1BS-500647761070_01_P001',
                 '15APR10184641-M1BS-500647760060_01_P001', '15FEB12183926-M1BS-500647760030_01_P001',
                 '15SEP28185751-M1BS-500647760050_01_P001', '15NOV29183732-M1BS-500647758020_01_P001',
                 '15AUG08184609-M1BS-500647758010_01_P001', '15FEB24183134-M1BS-500647759040_01_P001']

    # image_dir = r'C:/Users/rwagner/sandbox/core3d/sandboxData/materials_per_image_datasets'
    image_dir = r'C:\data\core3d\materials\bp_results\D2\mosaic'
    regex_str = "*_MTL_class_*_wgs84.tif_mosaic.tif"

    geotiff_paths = glob(os.path.join(image_dir, regex_str))

    image_set = set()
    for geotiff_path in geotiff_paths:
        # cat_id = geotiff_path.split('_MTL')[0][66:]
        cat_id = geotiff_path.split('_MTL')[0][-39:]
        image_set.add(cat_id)

    # outDir = "C:/Users/rwagner/sandbox/core3d/sandboxData/delivery19/bp_results/"
    outDir = "C:/Users/rwagner/sandbox/core3d/sandboxData/delivery19/bp_results_orig_mat_prob/"
    outDir = "C:/Users/rwagner/sandbox/core3d/sandboxData/delivery19/bp_results_datasets/"
    if not os.path.exists(outDir):
        os.makedirs(outDir)

    # class list and colors for WorldView data
    classIdx = [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    classList = [classList[i] for i in classIdx]

    # useClasses = [0,2,3,5,6,7,8,9,10]

    useClasses = [0,  # classIdx 0:   #0 grass
                  2,  # classIdx 3:   #1 water
                  3,  # classIdx 4:   #2 concrete
                  5,  # classIdx 6:   #3 asphalt
                  6,  # classIdx 7:   #4 metal
                  7,  # classIdx 8:   #5 solar panel
                  8,  # classIdx 9:   #6 tree
                  9,  # classIdx 10:  #7 polymer
                  10] # classIdx 11:  #8 shadow

    plotColors = np.zeros((len(useClasses),3))
    for i in range(len(useClasses)):
        plotColors[i,:] = visColors[classIdx[useClasses[i]],:]
    transitionOrder = [[1,0,6],  # natural - water, grass, tree
                       [3,2],    # manmade ground - asphalt, concrete
                       [4,7,5]]  # manmade roof - metal, polymer, solar panel

    natural = np.array([0,1,6])
    manmade = np.array([2,3,4,5,7])

elif imageType=='Landsat':

    imageList = sorted(glob("/Users/cara.perkins/Desktop/STR_Programs/SMART/results/NGA_Building/L*/"))
    outDir = "/Users/cara.perkins/Desktop/STR_Programs/SMART/results_bp/NGA_Building_lasso/"

    classIdx = [0,2,4,6,9,10,11]

    useClasses = [0,1,2,3,4,5,6] # grass, soil, concrete, asphalt, tree, polymer, shadow
    plotColors = np.zeros((len(useClasses), 3))
    for i in range(len(useClasses)):
        plotColors[i, :] = visColors[classIdx[useClasses[i]], :]
    transitionOrder = [[1, 0, 4],  # natural - soil,  grass, tree
                       [2, 3],  # manmade ground - concrete, brick, asphalt
                       [5]]  # manmade roof - metal, polymer, solar panel
    # transitionOrder = [[2,3,0,9], # natural - soil, water, grass, tree
    #                     [4,5,6],  # manmade ground - concrete, brick, asphalt
    #                     [7,10,8]] # manmade roof - metal, polymer, solar panel

    natural = np.array([0,1,4])
    manmade = np.array([2,3,5])

    # classIdx = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    #
    # useClasses = [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    # plotColors = np.zeros((len(useClasses), 3))
    # for i in range(len(useClasses)):
    #     plotColors[i, :] = visColors[classIdx[useClasses[i]], :]
    # transitionOrder = [[1, 2, 0, 8],  # natural - soil, water, grass, tree
    #                    [3, 4, 5],  # manmade ground - concrete, brick, asphalt
    #                    [6, 9, 7]]  # manmade roof - metal, polymer, solar panel
    # # transitionOrder = [[2,3,0,9], # natural - soil, water, grass, tree
    # #                     [4,5,6],  # manmade ground - concrete, brick, asphalt
    # #                     [7,10,8]] # manmade roof - metal, polymer, solar panel
    #
    # natural = np.array([0, 8])
    # manmade = np.array([3, 4, 5, 6, 7, 9, 10, 11])


numClass = len(useClasses)
shadowClass = numClass-1
jumpCost = [[0.0, 0.22, 0.52],  # natural jumps
            [0.52, 0.0, 0.22],  # manmade ground jumps
            [0.52, 0.22, 0.0]]  # manmade roof jumps

# Setup the label network
# jumpCostMat = np.zeros((len(classIdx)-1,len(classIdx)-1))
jumpCostMat = np.zeros((len(useClasses)-1,len(useClasses)-1))
for t0 in range(len(transitionOrder)):
    for t1 in range(len(transitionOrder)):
        print('t0 -> t1: {} -> {}'.format(t0, t1))
        jumpCostMat[np.meshgrid(transitionOrder[t0],transitionOrder[t1])] = jumpCost[t0][t1]
matJumpCost = jumpCostMat[[transitionOrder[0][0],transitionOrder[1][0],transitionOrder[2][0]]]
# matJumpCost = matJumpCost[:,useClasses[:-1]]
matJumpCost *= 10.0
spatialJumpCost = 1.0 #1.0 #0.0
spatialSameCost = 0.0 # 0.0
spatialWindowCost = 0.0
spatialWindowRad = np.int_(0)
temporalChangeCost = 5.0 # 2.0
temporalSameCost = 0.0 # 0.01

net = MaterialLabelNetwork(spatialJumpCost, spatialSameCost, spatialWindowCost, spatialWindowRad, \
                           temporalChangeCost, temporalSameCost, \
                           matJumpCost, useClasses, plotColors, transitionOrder)
net.setLabelParams(0, numClass)

# Compute the data cost
print("Creating data cost")
net.computeDataCost(imageList, image_dir, numClass, shadowClass)
print("Saving data cost")
net.saveDataCost(outDir+"vol.npy", outDir+"invalid.npy")
# print("Loading data cost")
# net.loadDataCost(outDir+"vol.npy", outDir+"invalid.npy")

print("Running belief propagation")
# net.changeDetectionMap(outDir,natural,manmade,0)

iter = 3
net.runBP(iter,outDir)

print("Processing change detection results")
net.changeDetectionMap(outDir,natural,manmade,iter)

print("Saving data cost")
net.saveDataCost(outDir+"vol_final.npy", outDir+"invalid_final.npy")

# fig,ax1 = plt.subplots(figsize=(25,4))
# a=ax1.plot(net._dataCost[:,64:67,66:69,1].reshape((len(imageList),-1)),label='Soil',color=plotColors[1,:])
# b=ax1.plot(net._dataCost[:,64:67,66:69,3].reshape((len(imageList),-1)),label='Asphalt',color=plotColors[3,:])
# plt.setp(a[1:],label='_')
# plt.setp(b[1:],label='_')
# plt.xlabel('Image #',fontsize='xx-large')
# plt.ylabel('Data Cost',fontsize='xx-large')
# plt.legend(fontsize='xx-large')
# plt.title('NGA Development',fontsize='xx-large')
# plt.savefig(outDir+"NGA.png")
# fig,ax2 = plt.subplots(figsize=(25,4))
# a=ax2.plot(net._dataCost[:, 94:96,29:33, 4].reshape((len(imageList),-1)),label='Tree', color=plotColors[4, :])
# b=ax2.plot(net._dataCost[:, 94:96,29:33, 3].reshape((len(imageList),-1)),label='Asphalt', color=plotColors[3, :])
# plt.setp(a[1:],label='_')
# plt.setp(b[1:],label='_')
# plt.xlabel('Image #',fontsize='xx-large')
# plt.ylabel('Data Cost',fontsize='xx-large')
# plt.legend(fontsize='xx-large')
# plt.title('Bus Development',fontsize='xx-large')
# plt.savefig(outDir+"BUS.png",fontsize='xx-large')
# plt.show()
# a=0