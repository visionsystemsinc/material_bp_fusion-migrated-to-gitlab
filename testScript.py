# AUTHOR: Thomas Pollard
# COMPANY: Systems & Technology Research
# DATE: 6/28/19

from glob import glob
import numpy as np
from TemporalDepthNetwork import *

# Height files and bounds
heightFiles = sorted(glob("/Users/cara.perkins/Desktop/STR_Programs/SatTel/data/rawDEM/*.tif"))
outDir = "/Users/cara.perkins/Desktop/STR_Programs/SatTel/results/beliefPropagation/rawDEMavg/50_5_3_avgAfter/"
gsd = 1.0
minZ, maxZ, stepZ, stdevZ = 40.0, 200.0, gsd, 10*gsd

# Setup the label network
spatialJumpCost = 50.0
spatialSameCost = 0.0
spatialWindowCost = 5.0
spatialWindowRad = np.int_(3)
temporalChangeCost = 10.0
temporalSameCost = 0.0

net = TemporalDepthNetwork()
net.setLabelParams(minZ, maxZ, stepZ, stdevZ)
# Compute the data cost
# print("Creating data cost")
# net.computeDataCost(heightFiles,dilution=0.1,averaging='cost')
# print("Saving the data cost")
# net.saveDataCost(outDir+"vol_avgAfter.npy", outDir+"invalid_avgAfter.npy", outDir+"timeStamps_avgAfter.npy")
print("Loading the data cost")
net.loadDataCost(outDir+"vol_avgAfter.npy", outDir+"invalid_avgAfter.npy", outDir+"timeStamps_avgAfter.npy")
net.initializeFactors(spatialJumpCost, spatialSameCost, spatialWindowCost, spatialWindowRad, \
                           temporalChangeCost, temporalSameCost=temporalSameCost)

print("Running belief propagation")
net.runBP(3,outDir)

