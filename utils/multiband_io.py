import os
import numpy as np

from osgeo import gdal, gdalconst, osr, gdalnumeric

def gdal_info(geotiff_path):
    if not os.path.exists(geotiff_path):
        raise FileExistsError(geotiff_path)

    if not os.path.isfile(geotiff_path):
        raise FileNotFoundError(geotiff_path)

    print('Loading <{}>'.format(geotiff_path))
    ds = gdal.Open(geotiff_path)

    prj = ds.GetProjection()
    print(prj)

    srs = osr.SpatialReference(wkt=prj)
    if srs.IsProjected:
        print(srs.GetAttrValue('projcs'))
    print(srs.GetAttrValue('geogcs'))

    print(ds.GetMetadata())

    print("[ RASTER BAND COUNT ]: {}".format(ds.RasterCount))

    GT = ds.GetGeoTransform()

    print('Raster Size {}x{}'.format(ds.RasterXSize, ds.RasterYSize))

    print('GeoTransform')
    print(GT)
    print('Origin at {}, {}'.format(GT[0], GT[3]))
    print('Pixel Size {}, {}'.format(GT[1], GT[5]))

    lrx = GT[0] + ds.RasterXSize * GT[1] + ds.RasterYSize * GT[2]
    lry = GT[3] + ds.RasterXSize * GT[4] + ds.RasterYSize * GT[5]
    print('Lower Right {}, {}'.format(lrx, lry))

    srcband = ds.GetRasterBand(1)
    print("[ NO DATA VALUE ] = {}".format(srcband.GetNoDataValue()))
    print("[ MIN ] = {}".format(srcband.GetMinimum()))
    print("[ MAX ] = {}".format(srcband.GetMaximum()))
    print("[ SCALE ] = {}".format(srcband.GetScale()))
    print("[ UNIT TYPE ] = {}".format(srcband.GetUnitType()))

    # Get raster statistics
    stats = srcband.GetStatistics(True, True)
    if stats is not None:
        # Print the min, max, mean, stdev based on stats index
        # print("[ STATS ] =  Minimum={:.3f}, Maximum={:.3f}, Mean={:.3f}, StdDev={:.3f}").format(stats[0], stats[1],
        #                                                                                         stats[2], stats[3])
        print(stats)

    raster_data = srcband.ReadAsArray()
    ndv = srcband.GetNoDataValue()
    raster_vals = raster_data[raster_data != ndv]
    min_val = np.amin(raster_vals)
    max_val = np.amax(raster_vals)
    print("Computed Min/Max={},{}".format(min_val, max_val))

    # Save gdal info in map
    gdal_info = {}
    gdal_info['GT'] = GT
    gdal_info['PRJ'] = prj
    gdal_info['GEOGCS'] = srs.GetAttrValue('geogcs')
    # gdal_info['meta'] = ds.GetMetadata()
    gdal_info['NoData Value'] = srcband.GetNoDataValue()

    # close file
    del(ds)

    return gdal_info

def get_raster_bands(geotiff_path):
    if not os.path.exists(geotiff_path):
        raise FileExistsError(geotiff_path)

    if not os.path.isfile(geotiff_path):
        raise FileNotFoundError(geotiff_path)

    return gdalnumeric.LoadFile(geotiff_path)
