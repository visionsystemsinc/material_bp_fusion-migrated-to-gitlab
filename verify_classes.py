import numpy as np

# STR class list and colors
classList = ['grass',  # 0
             'glass',  # 1
             'soil',  # 2
             'water',  # 3
             'concrete',  # 4
             'brick',  # 5
             'asphalt',  # 6
             'metal',  # 7
             'solar panel',  # 8
             'tree',  # 9
             'polymer',  # 10
             'shadow']  # 11


# STR -> T&E MATERIAL MAPPING
expected_labels = ["grass",        #0
                   "soil",         #1
                   "water",        #2
                   "concrete",     #3
                   "brick",        #4
                   "asphalt",      #5
                   "metal",        #6
                   "solar panel",  #7
                   "tree",         #8
                   "polymer",      #9
                   "shadow"]       #10
STR_to_TE = [
  [ 0,    0], # grass
  [ 1, None], # glass
  [ 2,    1], # soil
  [ 3,    2], # water
  [ 4,    3], # concrete
  [ 5,    4], # brick
  [ 6,    5], # asphalt
  [ 7,    6], # metal
  [ 8,    7], # solar panel
  [ 9,    8], # tree
  [10,    9], # polymer
  [11,   10]  # shadow
]

STR_to_TE_MAPPING = [x[1] for x in STR_to_TE]

if __name__ == '__main__':
  # material image to T&E numbering schema
  mapping = np.vectorize(lambda x: STR_to_TE_MAPPING[x])

  for i, j in STR_to_TE:
      if j is not None:
        print('{} -> {}'.format(classList[i], expected_labels[j]))
      else:
        print('{} -> None'.format(classList[i]))


  pass
