from utils import multiband_io

def show_info(geotiff_path):
    img_info = multiband_io.gdal_info(geotiff_path)


if __name__ == '__main__':
    # geotiff_path = r'C:/Users/rwagner/sandbox/core3d/sandboxData/delivery19/images/16NOV07165044-M1BS-500985234010_01_P001.tif'
    geotiff_path = r'C:/Users/rwagner/sandbox/core3d/sandboxData/delivery19/images/16NOV07165044-M1BS-500985234010_01_P001_uint8.tif'
    geotiff_path = r'C:/Users/rwagner/sandbox/core3d/sandboxData/materials_per_image/delivery_8_3_20_v16_16NOV07165044-M1BS-500985234010_01_P001_MTL_class_0_grass_wgs84.tif'

    geotiff_path = r'C:/Users/rwagner/sandbox/core3d/sandboxData/materials_per_image/delivery_8_3_20_v16_16SEP25165422-M1BS-500916291030_01_P001_MTL_class_0_grass_wgs84.tif'
    show_info(geotiff_path)

    material_prob = multiband_io.get_raster_bands(geotiff_path)


    pass