import os
from glob import glob
import numpy as np
import tifffile as tiff
from sortedcontainers import SortedSet

from MaterialLabelNetwork import MaterialLabelNetwork


# class list and colors
CLASS_LIST = ['grass',  # 0
             'glass',  # 1
             'soil',  # 2
             'water',  # 3
             'concrete',  # 4
             'brick',  # 5
             'asphalt',  # 6
             'metal',  # 7
             'solar panel',  # 8
             'tree',  # 9
             'polymer',  # 10
             'shadow']  # 11

# # STR
# allColors = {
#     'grass': [0, 1, 0],
#     'glass': [0, 0.5, 0.5],
#     'soil': [0.85, 0.8, 0.5], #[1, 1, 0], #
#     'water': [0, 0, 1],
#     'concrete': [0.66, 0.66, 0.66],
#     'brick': [1, 0, 0],
#     'asphalt': [0.33, 0.33, 0.33],
#     'metal': [0, 1, 1],
#     'solar panel': [0.54, 0.17, 0.87],
#     'tree': [0.13, 0.55, 0.13],
#     'polymer': [1, 1, 1],
#     'shadow': [0, 0, 0]
# }

# Core3D T&E
allColors = {
    'grass': [   0, 255,   0],
    'glass': [ 255,   0, 255],
    'soil':  [ 255, 255,   0],
    'water': [   0,   0, 255],
    'concrete': [ 255, 165,   0],
    'brick': [ 255,   0,   0],
    'asphalt': [ 128, 128, 128],
    'metal': [   0, 255, 255],
    'solar panel': [  96,   0, 191],
    'tree': [  38, 154,  38],
    'polymer': [ 255, 255, 255],
    'shadow': [0, 0, 0]
}

visColors = np.zeros((len(CLASS_LIST), 3))
for k, color in enumerate(CLASS_LIST):
    visColors[k, :] = np.array(allColors[color])/255.0


def get_image_catalog(image_dir, regex_str):
    geotiff_paths = glob(os.path.join(image_dir, regex_str))

    image_set = SortedSet()
    for geotiff_path in geotiff_paths:
        cat_id = geotiff_path.split('_MTL')[0][-39:]
        image_set.add(cat_id)

    return image_set


def retrieve_image_sets(images_root_dir):
    # build catalog of Panchromatic images
    vnir_images_dir = os.path.join(images_root_dir, 'MSI')
    vnir_image_set = SortedSet(os.listdir(vnir_images_dir))


    # build catalog of SWIR images
    swir_images_dir = os.path.join(images_root_dir, 'SWIR')
    swir_image_set = SortedSet(os.listdir(swir_images_dir))

    pass
    return vnir_image_set, swir_image_set


def get_vnir_material_prob_images(material_prob_image_set, vnir_image_set):
    vnir_material_prob_image_set = material_prob_image_set & vnir_image_set

    print('VNIR Image Set: {}'.format(vnir_material_prob_image_set))

    pass
    return vnir_material_prob_image_set


def get_swir_material_prob_images(material_prob_image_set, swir_image_set):
    swir_material_prob_image_set = material_prob_image_set & swir_image_set

    print('SWIR Image Set: {}'.format(swir_material_prob_image_set))

    pass
    return swir_material_prob_image_set


def run_bp_material_label_newtork(material_prob_image_dir, vnir_material_prob_image_set, swir_material_prob_image_set, vnir_weight,
                      swir_weight, n_bp_iterations, output_dir, use_zero_jump_cost_matrix=False, use_none_jump_cost_matrix=False):
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # class list and colors for WorldView data
    classIdx = [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    classList = [CLASS_LIST[i] for i in classIdx]

    useClasses = [0,  # classIdx 0:   #0 grass
                  2,  # classIdx 3:   #1 water
                  3,  # classIdx 4:   #2 concrete
                  5,  # classIdx 6:   #3 asphalt
                  6,  # classIdx 7:   #4 metal
                  7,  # classIdx 8:   #5 solar panel
                  8,  # classIdx 9:   #6 tree
                  9,  # classIdx 10:  #7 polymer
                  10] # classIdx 11:  #8 shadow

    plotColors = np.zeros((len(useClasses),3))
    for i in range(len(useClasses)):
        plotColors[i,:] = visColors[classIdx[useClasses[i]],:]
    transitionOrder = [[1,0,6],  # natural - water, grass, tree
                       [3,2],    # manmade ground - asphalt, concrete
                       [4,7,5]]  # manmade roof - metal, polymer, solar panel

    natural_classes = np.array([0,1,6])
    manmade_classes = np.array([2,3,4,5,7])


    #######################################
    numClass = len(useClasses)
    shadowClass = numClass - 1
    jumpCost = [[0.0, 0.22, 0.52],  # natural jumps
                [0.52, 0.0, 0.22],  # manmade ground jumps
                [0.52, 0.22, 0.0]]  # manmade roof jumps

    if use_zero_jump_cost_matrix:
        jumpCost = np.zeros((3,3))

    # Setup the label network
    jumpCostMat = np.zeros((len(useClasses) - 1, len(useClasses) - 1))
    for t0 in range(len(transitionOrder)):
        for t1 in range(len(transitionOrder)):
            print('t0 -> t1: {} -> {}'.format(t0, t1))
            jumpCostMat[tuple(np.meshgrid(transitionOrder[t0], transitionOrder[t1]))] = jumpCost[t0][t1]
    matJumpCost = jumpCostMat[[transitionOrder[0][0], transitionOrder[1][0], transitionOrder[2][0]]]
    matJumpCost *= 10.0
    spatialJumpCost = 1.0
    spatialSameCost = 0.0
    spatialWindowCost = 0.0
    spatialWindowRad = np.int_(0)
    temporalChangeCost = 5.0
    temporalSameCost = 0.0

    if use_none_jump_cost_matrix:
        matJumpCost = None
        transitionOrder = None

    # Create Material Label Network
    net = MaterialLabelNetwork(spatialJumpCost, spatialSameCost, spatialWindowCost, spatialWindowRad, \
                               temporalChangeCost, temporalSameCost, \
                               matJumpCost, useClasses, plotColors, transitionOrder)
    net.setLabelParams(0, numClass)

    # Compute the data cost
    print("Creating data cost")
    image_lists = [list(vnir_material_prob_image_set), list(swir_material_prob_image_set)]
    image_weights = [vnir_weight, swir_weight]
    net.computeDataCost(image_lists, image_weights, material_prob_image_dir, numClass, shadowClass)

    print("Running belief propagation")
    net.runBP(n_bp_iterations, output_dir)


if __name__ == '__main__':
    # Create spectral band image sets based on testbed directory structure
    images_root_dir = r'D:\data\core3d\fulltestbed\testbed\images\wpafb\WV3'
    # vnir_image_set, swir_image_set = retrieve_image_sets(images_root_dir)

    # hard coded list of images
    vnir_image_set = SortedSet(['14OCT26163007-M1BS-500171615020_01_P002', '14OCT26163009-M1BS-500171615020_01_P003', '16AUG30163358-M1BS-500849095010_01_P001', '16DEC14164726-M1BS-501044315030_01_P001', '16DEC14164741-M1BS-501044315060_01_P001', '16DEC14164756-M1BS-501044315040_01_P001', '16DEC14164903-M1BS-501044315050_01_P001', '16DEC14164913-M1BS-501044315020_01_P001', '16DEC14164924-M1BS-501044315010_01_P001', '16NOV07165023-M1BS-500985234030_01_P001', '16NOV07165033-M1BS-500985234020_01_P001', '16NOV07165044-M1BS-500985234010_01_P001', '16NOV07165055-M1BS-500985234050_01_P001', '16NOV07165105-M1BS-500985234040_01_P001', '16NOV13164717-M1BS-500969558010_01_P001', '16NOV13164808-M1BS-500969558010_01_P001', '16OCT07164831-M1BS-500941044020_01_P001', '16OCT07164845-M1BS-500941044040_01_P001', '16OCT07164856-M1BS-500941044030_01_P001', '16OCT07164906-M1BS-500941044050_01_P001', '16OCT07164917-M1BS-500941044010_01_P001', '16OCT14170046-M1BS-500910627010_01_P001', '16OCT19164337-M1BS-500952819010_01_P001', '16OCT26165543-M1BS-500959534010_01_P002', '16SEP13170005-M1BS-500879858010_01_P001', '16SEP19165733-M1BS-500901283010_01_P001', '16SEP25165352-M1BS-500916291040_01_P001', '16SEP25165407-M1BS-500916291010_01_P001', '16SEP25165422-M1BS-500916291030_01_P001', '16SEP25165437-M1BS-500916291020_01_P001', '16SEP25165448-M1BS-500910385010_01_P001', '17JAN01163905-M1BS-501073324070_01_P001', '17JAN01163959-M1BS-501073330070_01_P001'])
    swir_image_set = SortedSet(['16AUG30163357-A1BS-500849095010_01_P002', '16OCT14170045-A1BS-500910627010_01_P002', '16OCT19164335-A1BS-500952819010_01_P002', '16OCT26165541-A1BS-500959534010_01_P001', '16SEP13170007-A1BS-500879858010_01_P002', '16SEP19165732-A1BS-500901283010_01_P002'])

    material_prob_image_dir = r'C:\Users\rwagner\sandbox\core3d\sandboxData\delivery_8_26_20_v5\materials_per_image_datasets_tile_0' # non-weighted images

    regex_str = "*_MTL_class_*_wgs84.tif"
    material_prob_image_set = get_image_catalog(material_prob_image_dir, regex_str)

    vnir_material_prob_image_set = get_vnir_material_prob_images(material_prob_image_set, vnir_image_set)
    swir_material_prob_image_set = get_swir_material_prob_images(material_prob_image_set, swir_image_set)

    # Set weights for different spectral regimes
    swir_weight = 0.3
    vnir_weight = 1 - swir_weight
    n_bp_iterations = 3

    output_dir1 = r'C:/temp/material_bp_fusion_vnir_0.7_swir_0.3_delivery_8_26_20_v5_default/'
    run_bp_material_label_newtork(material_prob_image_dir, vnir_material_prob_image_set, swir_material_prob_image_set,
                            vnir_weight, swir_weight, n_bp_iterations, output_dir1)

    output_dir2 = r'C:/temp/material_bp_fusion_vnir_0.7_swir_0.3_delivery_8_26_20_v5_zero_matJumpCost/'
    run_bp_material_label_newtork(material_prob_image_dir, vnir_material_prob_image_set, swir_material_prob_image_set,
                            vnir_weight, swir_weight, n_bp_iterations, output_dir2, use_zero_jump_cost_matrix=True)

    output_dir3 = r'C:/temp/material_bp_fusion_vnir_0.7_swir_0.3_delivery_8_26_20_v5_none_matJumpCost/'
    run_bp_material_label_newtork(material_prob_image_dir, vnir_material_prob_image_set, swir_material_prob_image_set,
                                  vnir_weight, swir_weight, n_bp_iterations, output_dir3, use_zero_jump_cost_matrix=False,
                                  use_none_jump_cost_matrix=True)


    n_images = len(vnir_material_prob_image_set)+len(swir_material_prob_image_set)
    fused_material_filename = "output_{:02d}_{:02d}.tif".format(n_bp_iterations, n_images-1)

    fused_material_path1 = os.path.join(output_dir1, fused_material_filename)
    fused_material1 = tiff.imread(fused_material_path1)
    fused_material_path2 = os.path.join(output_dir2, fused_material_filename)
    fused_material2 = tiff.imread(fused_material_path2)
    fused_material_path3 = os.path.join(output_dir3, fused_material_filename)
    fused_material3 = tiff.imread(fused_material_path3)

    sad_12 = np.sum(np.abs(fused_material1 - fused_material2))
    sad_13 = np.sum(np.abs(fused_material1 - fused_material3))

    print('SAD(default, zero matJumpCost) = {}'.format(sad_12))
    print('SAD(default, NONE matJumpCost) = {}'.format(sad_13))

